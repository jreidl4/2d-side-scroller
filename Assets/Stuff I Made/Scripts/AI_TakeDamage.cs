﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_TakeDamage : MonoBehaviour {

	private Animator anim;
	private AI_Robot1 ai;

	public int lifeCount;

	private CircleCollider2D hitCheck;

	void Start ()
	{
		anim = GetComponentInParent<Animator> ();
		ai = GetComponentInParent<AI_Robot1> ();

		hitCheck = GetComponent<CircleCollider2D> ();
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Player")
		{
			Rigidbody2D playerBody = other.GetComponent<Rigidbody2D> ();

			if (lifeCount == 0) {
				anim.SetTrigger ("isDead");
				ai.isAlive = false;
				hitCheck.enabled = false;
				playerBody.AddForce (new Vector2 (0, 500), ForceMode2D.Force);
				Debug.Log ("Player Killed Robot");
			}

			if (lifeCount > 0)
			{
				anim.SetTrigger ("isHurt");
				ai.isHurt = true;
				lifeCount -= 1;
				playerBody.AddForce (new Vector2 (0, 500), ForceMode2D.Force);
				Debug.Log ("Player Hurt Robot");
			}
		}
	}
}
