﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climbable : MonoBehaviour {

	private GameObject player;

	private PlayerMove2 playerMove2;

	void Start ()
	{
		player = GameObject.Find ("Player");
		playerMove2 = player.GetComponentInParent<PlayerMove2> ();

	}

	void OnTriggerEnter2D (Collider2D other)
	{

		if (other.tag == "WallCheck" && Input.GetAxis("Vertical") == 1f && playerMove2.sitting == false)
		{
			Debug.Log ("On Wall");
			playerMove2.onWall = true;
		}

	}

	void OnTriggerStay2D (Collider2D other)
	{

		if (other.tag == "Climbable" && Input.GetAxis("Vertical") == 1f && playerMove2.sitting == false)
		{
			//Debug.Log ("On Wall");
			playerMove2.onWall = true;
		}

		/*
		if (other.tag == "Climbable" && Input.GetAxis("Vertical") <= -0.5f && playerMove2.grounded == true)
		{
			Debug.Log ("Left Wall");
			playerMove2.onWall = false;
		}
		*/

	}

	void OnTriggerExit2D (Collider2D other)
	{
		if (other.tag == "Climbable" && playerMove2.onWall == true)
		{
			Debug.Log ("Left Wall");
			playerMove2.onWall = false;
		}
	}

}
