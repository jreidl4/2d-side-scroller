﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Patrol : MonoBehaviour {

	private Animator anim;

	private AI_Robot1 ai;

	void Start ()
	{
		anim = GetComponentInParent<Animator> ();
		ai = GetComponentInParent<AI_Robot1> ();

	}
	
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Player")
		{
			anim.SetBool ("isAttacking", true);
		}

		if (other.ToString() == "Platform 8 (UnityEngine.EdgeCollider2D)" || other.ToString() == "Platform 8(Clone) (UnityEngine.EdgeCollider2D)")
		{
			ai.StartCoroutine (ai.Flip());
		}
	}
}
