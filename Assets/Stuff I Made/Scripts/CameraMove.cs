﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {

	public Transform target;
	public float smoothing;

	private Vector3 offset;
	private Vector3 offsetOrig;
	private Vector3 lastTargetPos;
	private GameController gc;

	private GameObject player;
	private Rigidbody2D playerBody;

	public bool lookAheadMouse;

	public int lookAheadX;
	public int lookAheadY;

	private int mouseScrollZoneX;
	private int mouseScrollZoneY;

	private int screenWidth;
	private int screenHeight;

	void Start () 
	{
		gc = GameObject.Find ("GameController").GetComponent<GameController> ();

		player = GameObject.Find ("Player");
		playerBody = player.GetComponent<Rigidbody2D> ();

		offset = transform.position - target.position;
		offsetOrig = offset;

		screenWidth = Screen.width;
		screenHeight = Screen.height;

		mouseScrollZoneX = screenWidth / 3;
		mouseScrollZoneY = screenWidth / 4;

	}

	void Update ()
	{

		if (playerBody.velocity.x == 0 && playerBody.velocity.y == 0 && Input.GetButton("Look"))
		{
			if (lookAheadMouse == true)
			{
				if (Input.mousePosition.x > screenWidth - mouseScrollZoneX) {
					offset = new Vector3 (Mathf.Clamp (transform.position.x + lookAheadX, lookAheadX, offsetOrig.x + lookAheadX), offset.y, offset.z);
				}

				if (Input.mousePosition.x < 0 + mouseScrollZoneX) {
					offset = new Vector3 (Mathf.Clamp (transform.position.x - lookAheadX, -lookAheadX, offsetOrig.x + lookAheadX), offset.y, offset.z);
				}

				if (Input.mousePosition.y > screenHeight - mouseScrollZoneY) {
					offset = new Vector3 (offset.x, Mathf.Clamp (transform.position.y + lookAheadY, lookAheadY, offsetOrig.y + lookAheadY), offset.z);
				}

				if (Input.mousePosition.y < 0 + mouseScrollZoneY) {
					offset = new Vector3 (offset.x, Mathf.Clamp (transform.position.y - lookAheadY, -lookAheadY, offsetOrig.y + lookAheadY), offset.z);
				}
			}

			if (Mathf.Abs(Input.GetAxis ("Joystick X")) > 0)
			{
				offset = new Vector3 (Mathf.Clamp(offset.x + (Input.GetAxis ("Joystick X") * lookAheadX), offsetOrig.x - lookAheadX, offsetOrig.x + lookAheadX), offset.y, offset.z);
			}

			if (Mathf.Abs(Input.GetAxis ("Joystick Y")) > 0)
			{
				offset = new Vector3 (offset.x, Mathf.Clamp(offset.y + (Input.GetAxis ("Joystick Y") * lookAheadY), offsetOrig.y - lookAheadY, offsetOrig.y + lookAheadY), offset.z);
			}
		}

		if (Input.GetButtonUp ("Look"))
		{
			offset = new Vector3 (offsetOrig.x, offsetOrig.y, offsetOrig.z);
		}
	}

	void FixedUpdate ()
	{
		if (gc.GC_cameraLock == false) 
		{
			Vector3 targetCameraPosition = target.position + offset;
			transform.position = Vector3.Lerp (transform.position, targetCameraPosition, smoothing * Time.deltaTime);
			lastTargetPos = target.position;
		} 
		else if (gc.GC_cameraLock == true)
		{
			Vector3 targetCameraPosition = lastTargetPos + offset;
			transform.position = Vector3.Lerp (transform.position, targetCameraPosition, smoothing * Time.deltaTime);
		}

		if (Mathf.Abs(playerBody.velocity.x) > 0 || Mathf.Abs(playerBody.velocity.y) > 0)
		{
			offset = new Vector3 (offsetOrig.x, offsetOrig.y, offsetOrig.z);
			Vector3 targetCameraPosition = lastTargetPos + offset;
			transform.position = Vector3.Lerp (transform.position, targetCameraPosition, (smoothing / smoothing) * Time.deltaTime);
		}
	}
}