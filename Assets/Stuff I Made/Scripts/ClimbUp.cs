﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbUp : MonoBehaviour {

	private GameObject player;
	private Animator anim;
	private Rigidbody2D playerBody;
	private Component playerMove;
	private Component pm;

	void Start()
	{
		player = GameObject.Find ("Player");
		anim = player.GetComponent<Animator> ();
		playerBody = player.GetComponent<Rigidbody2D>();
	}

	/*
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.name == "WallCheck")
		{
			playerBody.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
		}
	}
	*/

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.name == "WallCheck")
		{
			if (Input.GetAxis ("Vertical") == 1)
			{
				Debug.Log ("Start Climb Up");
				anim.SetTrigger ("ClimbUpStart");
			}
		}
	}

}
