﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinZone : MonoBehaviour {

	private Collider2D playerBody;
	private Collider2D winZone;
	private GameController gc;

	void Start ()
	{
		playerBody = GameObject.Find ("Player").GetComponent<CapsuleCollider2D> ();
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");

		if (gameControllerObject != null)
		{
			gc = gameControllerObject.GetComponent <GameController> ();
		}

		if (gc == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other == playerBody)
		{
			gc.GC_winFlag = true;
			Debug.Log ("You Won");
		}
	}

}
