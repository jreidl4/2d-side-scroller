﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public float moveForce;
	public float jumpHeight;
	public float xSpeedLimit;
	public float ySpeedLimit;
	public float climbSpeed;

	public Transform groundCheck;
	public LayerMask whatIsGround;

	public bool isHurt;

	public Rigidbody2D rb;

	private CapsuleCollider2D playerHead;
	private BoxCollider2D playerBody;
	private CircleCollider2D playerFeet;

	private Vector2 headOffsetOrig;
	private Vector2 bodyOffsetOrig;
	private Vector2 feetOffsetOrig;

	private Animator anim;

	private Vector3 playerScale;

	private float groundRadius;
	private float gravity;

	private float moveHorizontal;
	private float moveVertical;

	private float xSpeedLimitOrig;

	private bool grounded;
	private bool onWall;
	private bool sitting;
	private bool facingRight;
	private bool doubleJump;

	private GameObject playerWallCheck;

	void Start ()
	{
		rb = GetComponent<Rigidbody2D> ();
		playerHead = GetComponent<CapsuleCollider2D> ();
		playerBody = GetComponent<BoxCollider2D>();
		playerFeet = GetComponent<CircleCollider2D> ();

		headOffsetOrig = playerHead.offset;
		bodyOffsetOrig = playerBody.offset;
		feetOffsetOrig = playerFeet.offset;

		anim = GetComponent<Animator> ();

		xSpeedLimitOrig = xSpeedLimit;

		playerWallCheck = GameObject.Find ("WallCheck");

		facingRight = true;
		//onWall = false;
		sitting = false;
		groundRadius = 0.2f;
		gravity = rb.gravityScale;
		//doubleJump = false;

	}

	void Update()
	{
		if (isHurt == false)
		{
			// Uncomment the below line to enable double jumping
			//if ((grounded || !doubleJump) && Input.GetButtonDown("Jump"))
			if (grounded && Input.GetButtonDown ("Jump"))
			{
				anim.SetBool ("isGrounded", false);
				rb.AddForce (new Vector2 (0, jumpHeight), ForceMode2D.Force);
			}

			/*
			// Uncomment this block to enable double jumping
			if (!doubleJump && !grounded)
			{
				doubleJump = true;
			}
			*/

			/*
			if (onWall == false)
			{
				rb.gravityScale = gravity;
			}
			*/

			if (grounded != true)
			{
				anim.SetBool ("isSitting", false);
				xSpeedLimit = xSpeedLimitOrig;
				sitting = false;
			}

			/*
			if (onWall && Input.GetButtonDown ("Jump"))
			{
				anim.SetBool ("isOnWall", false);
				rb.constraints = RigidbodyConstraints2D.FreezeRotation;
				rb.gravityScale = gravity;
				rb.AddForce (new Vector2 (playerScale.x * -50, jumpHeight), ForceMode2D.Force);
			}

			if (onWall && Mathf.Abs (Input.GetAxis ("Vertical")) > 0)
			{
				anim.SetBool ("isClimbing", true);
				float moveVertical = Input.GetAxis ("Vertical") * climbSpeed;
				Vector2 climbDirection = new Vector2 (rb.position.x, rb.position.y + moveVertical * Time.deltaTime);
				rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
				rb.MovePosition (climbDirection);
				rb.gravityScale = 0f;
			}
				
			if (onWall && Mathf.Abs (Input.GetAxis ("Horizontal")) > 0)
			{
				anim.SetBool ("isClimbing", true);
				float moveHorizontal = Input.GetAxis ("Horizontal") * climbSpeed;
				Vector2 climbDirection = new Vector2 (rb.position.x + moveHorizontal *Time.deltaTime, rb.position.y);
				rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionY;
				rb.MovePosition (climbDirection);
				rb.gravityScale = 0f;
			}


			if (onWall && Mathf.Abs (Input.GetAxis ("Vertical")) == 0)
			{
				anim.SetBool ("isClimbing", false);
			}
			*/

			if (grounded && sitting == false && /* onWall == false && */ Input.GetAxis ("Vertical") == -1)
			{
				Debug.Log ("Sit Down");
				anim.SetBool ("isSitting", true);
				playerHead.offset = new Vector2 (playerHead.offset.x, 0.1f);
				playerBody.offset = new Vector2 (playerBody.offset.x, -0.1f);
				xSpeedLimit = xSpeedLimit * 0.1f;
				sitting = true;
			}

			if (sitting == true && Input.GetAxis ("Vertical") == 1)
			{
				Debug.Log ("Get Up");
				anim.SetBool ("isSitting", false);
				anim.SetBool ("isScooching", false);
				playerHead.offset = headOffsetOrig;
				playerBody.offset = bodyOffsetOrig;
				xSpeedLimit = xSpeedLimitOrig;
				sitting = false;
			}
		}

	}

	void FixedUpdate()
	{
		if (isHurt == false)
		{
			moveHorizontal = Input.GetAxis ("Horizontal");
			moveVertical = Input.GetAxis ("Vertical");
			grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
			//onWall = anim.GetBool ("isOnWall");
			anim.SetBool ("isGrounded", grounded);
			anim.SetFloat ("ySpeed", rb.velocity.y);
			anim.SetFloat ("xSpeed", Mathf.Abs(rb.velocity.x));

			if ( Mathf.Abs(rb.velocity.x * Time.deltaTime) > 0.01f )
			{
				if (sitting)
				{
					anim.SetBool ("isScooching", true);
				}

				if (sitting == false)
				{
					anim.SetBool ("isRunning", true);
				}
			}
			else if ( Mathf.Abs(rb.velocity.x * Time.deltaTime) <= 0.01f )
			{
				if (sitting)
				{
					anim.SetBool ("isScooching", false);
				}

				if (sitting == false)
				{
					anim.SetBool ("isRunning", false);
				}
			}	

			Vector2 move = new Vector2 (moveHorizontal, 0.0f);
			rb.AddForce (move * moveForce);
			rb.velocity = new Vector2 (Mathf.Clamp (rb.velocity.x, -xSpeedLimit, xSpeedLimit), Mathf.Clamp (rb.velocity.y, -ySpeedLimit, ySpeedLimit));

			if (moveHorizontal > 0 && !facingRight)
			{
				Flip ();
			}
			else if (moveHorizontal < 0 && facingRight)
			{
				Flip ();
			}
			/*
			if (grounded)
			{
				doubleJump = false;
			}
			*/
		}
	}

	void Flip()
	{
		facingRight = !facingRight;
		playerScale = transform.localScale;
		playerScale.x *= -1;
		transform.localScale = playerScale;
	}

	/*
	IEnumerator ClimbUp()
	{

		Debug.Log ("Climb Up Started");

		for (float f = 0; f <= 25; f++)
		{

			rb.gravityScale = 0;

			if (f < 10 )
			{
				rb.constraints = RigidbodyConstraints2D.FreezeAll;
			}
				
			if (f >= 10 && f < 20)
			{
				rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
				rb.MovePosition (new Vector2 (rb.position.x, rb.position.y + 0.125f));
			}

			if (f >= 20 && f < 25)
			{
				rb.constraints = RigidbodyConstraints2D.FreezeRotation;
				rb.MovePosition (new Vector2 (rb.position.x + 0.1f * transform.localScale.x, rb.position.y));
			}
				
			yield return new WaitForEndOfFrame();

		}

		anim.ResetTrigger ("ClimbUpStart");
		rb.gravityScale = gravity;

		Debug.Log ("Climb Up Ended");
	}
	*/

	IEnumerator GetUp()
	{
		rb.constraints = RigidbodyConstraints2D.FreezeAll;

		for (float f = 0; f <= 30; f++)
		{
			yield return new WaitForEndOfFrame();
		}

		rb.constraints = RigidbodyConstraints2D.FreezeRotation;
	}

	public IEnumerator TakeDamage()
	{
		anim.SetBool ("isRunning", false);

		for (float f = 0; f <= 30; f++)
		{
			yield return new WaitForEndOfFrame();
		}

		isHurt = false;
		anim.ResetTrigger ("isHurt");
	}

}
