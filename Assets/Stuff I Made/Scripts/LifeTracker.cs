﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeTracker : MonoBehaviour {

	private Text text;
	private GameController gc;

	private int lives;

	void Start ()
	{
		text = GetComponent <Text> ();
		gc = GameObject.Find ("GameController").GetComponent<GameController> ();

		text.text += gc.GC_lives;

	}

	void Update ()
	{
		text.text = "Lives: " + gc.GC_lives;
	}

}
