﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove2 : MonoBehaviour {

	public float moveForce;
	public float jumpHeight;
	public float xSpeedLimit;
	public float ySpeedLimit;
	public float climbSpeed;

	public Transform groundCheck;
	public LayerMask whatIsGround;

	public Transform wallCheck;

	public Transform hitBox;
	private CapsuleCollider2D playerHitBox;
	private Vector2 playerHitBoxOffsetOrig;
	private Vector2 playerHitBoxSizeOrig;

	public bool isHurt;
	public bool grounded;
	public bool onWall;
	public bool sitting;

	public Rigidbody2D rb;

	private Animator anim;

	private Vector3 playerScale;

	private float groundRadius;
	private float gravity;

	private float moveHorizontal;
	private float moveVertical;

	private float xSpeedLimitOrig;
	private float ySpeedLimitOrig;

	private bool facingRight;

	void Start ()
	{
		rb = GetComponent<Rigidbody2D> ();

		playerHitBox = hitBox.GetComponent<CapsuleCollider2D> ();

		playerHitBoxOffsetOrig = playerHitBox.offset;
		playerHitBoxSizeOrig = playerHitBox.size;

		anim = GetComponent<Animator> ();

		xSpeedLimitOrig = xSpeedLimit;

		facingRight = true;
		sitting = false;
		groundRadius = 0.2f;
		gravity = rb.gravityScale;

	}

	void Update()
	{
		//If the player isn't taking damage, they can do stuff

		if (isHurt == false)
		{

			if (grounded && !onWall)
			{
				//If the player is on the ground, use these settings

				anim.SetBool ("isOnWall", false);
				anim.SetBool ("isClimbing", false);
				anim.SetBool ("isGrounded", true);
				anim.ResetTrigger ("startJump");

				if (!sitting)
				{

					if (Input.GetButtonDown ("Jump"))
					{
						//The player will jump

						anim.SetBool ("isGrounded", false);
						anim.SetTrigger ("startJump");
						rb.AddForce (new Vector2 (0, jumpHeight), ForceMode2D.Force);
					}

					//If the player is standing, use these settings
					
					anim.SetBool ("isSitting", false);

					if (Input.GetAxis ("Vertical") == -1f)
					{
						//The player sits down if they push down on the controller

						Debug.Log ("Sit Down");
						anim.SetBool ("isSitting", true);
						playerHitBox.offset = new Vector2 (playerHitBox.offset.x, -0.25f);
						playerHitBox.size = new Vector2 (playerHitBox.size.x, 1.4f);
						xSpeedLimit = xSpeedLimit * 0.1f;
						sitting = true;
					}
				}

				if (sitting)
				{
					//If the player is sitting, use these settings

					//anim.SetBool ("isSitting", true);

					if (Input.GetButton ("Jump") || Input.GetAxis ("Vertical") == 1f)
					{
						//The player will stand up

						Debug.Log ("Get Up");
						anim.SetBool ("isSitting", false);
						anim.SetBool ("isScooching", false);
						playerHitBox.offset = playerHitBoxOffsetOrig;
						playerHitBox.size = playerHitBoxSizeOrig;
						xSpeedLimit = xSpeedLimitOrig;
						sitting = false;
					}
				}
			}

			if (!grounded)
			{
				//If the player is in the air, use these settings

				anim.SetBool ("isSitting", false);
				anim.SetBool ("isRunning", false);
				anim.SetBool ("isGrounded", false);
				sitting = false;
				xSpeedLimit = xSpeedLimitOrig;
			}

			if (onWall)
			{
				//If the player is on a wall, use these settings

				rb.constraints = RigidbodyConstraints2D.FreezeAll;

				anim.SetBool ("isClimbing", false);


				if (Input.GetButton ("Jump"))
				{
					//The player will jump off the wall

					Debug.Log ("Wall Jump");
					onWall = false;
					rb.constraints = RigidbodyConstraints2D.FreezeRotation;
					rb.gravityScale = gravity;
					rb.AddForce (new Vector2 (playerScale.x * -25, jumpHeight * 0.67f), ForceMode2D.Force);
				}


				if (Mathf.Abs (Input.GetAxis ("Vertical")) > 0)
				{
					//The player can move vertically on a wall

					anim.SetBool ("isClimbing", true);
					float moveVertical = Mathf.Lerp(0, Input.GetAxis ("Vertical") * climbSpeed, climbSpeed * 0.5f);
					//float moveHorizontal =  Mathf.Lerp(0, Input.GetAxis ("Horizontal") * climbSpeed, climbSpeed * 0.5f);
					Vector2 climbDirection = new Vector2 (rb.position.x /* + moveHorizontal * Time.deltaTime */, rb.position.y + moveVertical * Time.deltaTime);
					rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
					rb.MovePosition (climbDirection);
					rb.gravityScale = 0f;
					//Debug.Log ("Climbing");
				}

				/*
				if (Mathf.Abs (Input.GetAxis ("Horizontal")) > 0 )
				{
					//The player can move horizontally on a wall

					anim.SetBool ("isClimbing", true);
					//float moveVertical = Mathf.Lerp(0, Input.GetAxis ("Vertical") * climbSpeed, climbSpeed * 0.5f);
					float moveHorizontal =  Mathf.Lerp(0, Input.GetAxis ("Horizontal") * climbSpeed, climbSpeed * 0.5f);
					Vector2 climbDirection = new Vector2 (rb.position.x + moveHorizontal * Time.deltaTime, rb.position.y + moveVertical * Time.deltaTime);
					rb.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionY;
					rb.MovePosition (climbDirection);
					rb.gravityScale = 0f;
					//Debug.Log ("Climbing");
				}
				*/

			}

			if (!onWall)
			{
				//Use these settings if the player is not on a wall

				anim.SetBool ("isClimbing", false);
				anim.SetBool ("isOnWall", false);
				rb.constraints = RigidbodyConstraints2D.FreezeRotation;
				rb.gravityScale = gravity;
			}

		}

	}

	void FixedUpdate()
	{

		//If the player isn't hurt, they can move

		if (isHurt == false)
		{
			//Get the controller input to move the player

			moveHorizontal = Input.GetAxis ("Horizontal");
			moveVertical = Input.GetAxis ("Vertical");

			//Get character/world interaction data to set animation states

			grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);

			if (onWall)
			{
				grounded = false;
			}

			anim.SetBool ("isOnWall", onWall);
			anim.SetBool ("isGrounded", grounded);
			anim.SetFloat ("ySpeed", rb.velocity.y);
			anim.SetFloat ("xSpeed", Mathf.Abs(rb.velocity.x));

			//This section controls the animation states between idle/running, and sitting/scooching, based on the X velocity

			if ( Mathf.Abs(rb.velocity.x * Time.deltaTime) > 0.01f )
			{
				if (sitting)
				{
					anim.SetBool ("isScooching", true);
				}

				if (sitting == false)
				{
					anim.SetBool ("isRunning", true);
				}
			}
			else if ( Mathf.Abs(rb.velocity.x * Time.deltaTime) <= 0.01f )
			{
				if (sitting)
				{
					anim.SetBool ("isScooching", false);
				}

				if (sitting == false)
				{
					anim.SetBool ("isRunning", false);
				}
			}

			//This part moves the player

			Vector2 move = new Vector2 (moveHorizontal, 0.0f);
			rb.AddForce (move * moveForce);
			rb.velocity = new Vector2 (Mathf.Clamp (rb.velocity.x, -xSpeedLimit, xSpeedLimit), Mathf.Clamp (rb.velocity.y, -ySpeedLimit, ySpeedLimit));

			//This determines which way the character is facing

			if (!onWall && moveHorizontal > 0 && !facingRight)
			{
				Flip ();
			}
			else if (!onWall && moveHorizontal < 0 && facingRight)
			{
				Flip ();
			}

			//Uncomment block below to enable double jumping
			/*
			if (grounded)
			{
				doubleJump = false;
			}
			*/
		}
	}
		
	void Flip()
	{
		//Flips the character

		facingRight = !facingRight;
		playerScale = transform.localScale;
		playerScale.x *= -1;
		transform.localScale = playerScale;
	}

	/*
	IEnumerator ClimbUp()
	{
	
		//WILL NOT BE USED LATER
		//Use to put player on top of prefab platform

		Debug.Log ("Climb Up Started");

		for (float f = 0; f <= 25; f++)
		{

			rb.gravityScale = 0;

			if (f < 10 )
			{
				rb.constraints = RigidbodyConstraints2D.FreezeAll;
			}
				
			if (f >= 10 && f < 20)
			{
				rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
				rb.MovePosition (new Vector2 (rb.position.x, rb.position.y + 0.125f));
			}

			if (f >= 20 && f < 25)
			{
				rb.constraints = RigidbodyConstraints2D.FreezeRotation;
				rb.MovePosition (new Vector2 (rb.position.x + 0.1f * transform.localScale.x, rb.position.y));
			}
				
			yield return new WaitForEndOfFrame();

		}

		anim.ResetTrigger ("ClimbUpStart");
		rb.gravityScale = gravity;

		Debug.Log ("Climb Up Ended");
	}
	*/

	IEnumerator playerJump()
	{
		anim.SetTrigger ("startJump");

		for (float f = 0; f <= 30; f++)
		{
			grounded = false;
			yield return new WaitForEndOfFrame();
		}

		anim.ResetTrigger ("startJump");

	}

	public IEnumerator TakeDamage()
	{
		//Freeze the player animation while they take damage

		anim.SetBool ("isRunning", false);

		for (float f = 0; f <= 30; f++)
		{
			yield return new WaitForEndOfFrame();
		}

		isHurt = false;
		anim.ResetTrigger ("isHurt");
	}

}
