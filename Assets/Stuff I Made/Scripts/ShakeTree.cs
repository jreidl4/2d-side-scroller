﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeTree : MonoBehaviour {

	private GameObject player;

	public GameObject Artwork;

	private Animator anim;

	private ParticleSystem leaves;

	void Start ()
	{

		player = GameObject.Find ("Player");

		anim = Artwork.GetComponent<Animator> ();

		leaves = GetComponentInChildren<ParticleSystem> ();
		
	}
	

	void OnCollisionEnter2D(Collision2D other)
	{

		if (other.gameObject == player)
		{
			anim.SetTrigger ("ShakeTree");
			leaves.Play();
			Debug.Log ("Shake Tree");
		}
	}

}
