﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public bool GC_playerDead;
	public bool GC_playerActive;
	public bool GC_cameraLock;
	public bool GC_winFlag;

	public int GC_lives;

	private DeathByDrop dbd;
	private Rigidbody2D playerRB;
	private Vector3 startPosition;

	void Start ()
	{
		GC_playerDead = false;
		GC_playerActive = true;
		GC_winFlag = false;

		playerRB = GameObject.Find ("Player").GetComponent<Rigidbody2D> ();
		dbd = GameObject.Find ("GameFloor").GetComponent<DeathByDrop> ();

		startPosition = playerRB.transform.position;

		Debug.Log ("GameController Started");
	}

	void Update ()
	{
		GC_playerActive = !dbd.DBD_playerDead;

		if (GC_lives == 0 && dbd.DBD_playerDead == true)
		{
			GC_cameraLock = true;
			Invoke ("RestartLevel", 2);

		}
		if ( GC_lives > 0 && dbd.DBD_playerDead == true)
		{
			GC_cameraLock = true;
			dbd.DBD_playerDead = false;
			Invoke ("ResetPlayer", 1);
			Debug.Log ("Lost a Life");
		}

		if (GC_winFlag == true)
		{
			GC_cameraLock = true;
			Invoke ("RestartLevel", 2);
		}
	}

	void RestartLevel ()
	{
		SceneManager.LoadScene ("Setup");
		GC_lives = 3;

		Debug.Log ("Restarted");
	}

	void ResetPlayer()
	{
		playerRB.transform.position = startPosition;
		GC_playerActive = true;
		GC_playerDead = false;
		GC_cameraLock = false;
		GC_lives -= 1;

		Debug.Log ("Reset");
	}

}
