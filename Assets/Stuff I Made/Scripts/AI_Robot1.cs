﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Robot1 : MonoBehaviour {

	public float moveForce;
	public float xSpeedLimit;
	public float attackPower;
	public float maxEnergy;

	private float energy;

	private Rigidbody2D rb;
	private CircleCollider2D wheelHitBox;
	private EdgeCollider2D bodyHitBox;
	private CapsuleCollider2D triggerCheck;

	private Vector3 robotScale;

	private Animator anim;

	private bool facingRight;
	private bool idleCheck;

	public bool isAlive;
	public bool isHurt;

	private GameObject player;

	void Start () {

		rb = GetComponent<Rigidbody2D> ();
		wheelHitBox = GetComponent<CircleCollider2D> ();
		bodyHitBox = GetComponent<EdgeCollider2D>();

		anim = GetComponent<Animator> ();
		triggerCheck = GetComponentInChildren<CapsuleCollider2D> ();

		player = GameObject.Find ("Player");

		facingRight = false;

		float offsetX = Random.Range (-1f, 1f);

		rb.position = new Vector2 (rb.position.x + offsetX, rb.position.y);

		energy = maxEnergy;

		isAlive = true;
		
	}

	void Update()
	{
		if (isAlive == true)
		{
			if (energy <= 0) {
				idleCheck = true;
			}

			if (energy >= maxEnergy) {
				idleCheck = false;
			}

			if (Mathf.Abs (rb.velocity.x) > 0) {
				anim.SetBool ("isRolling", true);
			}

			if (Mathf.Abs (rb.velocity.x) == 0) {
				anim.SetBool ("isRolling", false);
			}
		}

		if (isAlive == false)
		{
			rb.constraints = RigidbodyConstraints2D.FreezeAll;
			wheelHitBox.enabled = false;
			bodyHitBox.enabled = false;
			StopAllCoroutines ();
		}

		if (isHurt == true)
		{
			StartCoroutine (TakeDamage ());
		}
	}

	void FixedUpdate()
	{
		if (idleCheck == true && isAlive == true && isHurt == false)
		{
			StartCoroutine (Idle ());
		}

		if (idleCheck == false && isAlive == true && isHurt == false)
		{
			StartCoroutine (Roll ());
		}
	}

	public IEnumerator Flip()
	{	
		StopCoroutine (Roll ());
		StartCoroutine (Idle ());
		facingRight = !facingRight;
		robotScale = transform.localScale;
		robotScale.x *= -1;
		transform.localScale = robotScale;
		yield return new WaitForSeconds(1);
	}

	IEnumerator Attack()
	{
		for (float f = 0; f <= 30; f++)
		{
			StopCoroutine (Idle ());
			StopCoroutine (Roll ());
			if (f == 30)
			{
				if (triggerCheck.IsTouching (player.GetComponent<Collider2D> ())) {
					Debug.Log ("Robot Hit Player");
					PlayerMove playerMove = player.GetComponent<PlayerMove> ();
					Animator playerAnim = player.GetComponent<Animator> ();
					playerAnim.SetTrigger ("isHurt");
					playerMove.isHurt = true;
					playerMove.StartCoroutine (playerMove.TakeDamage ());
					Rigidbody2D playerBody = player.GetComponent<Rigidbody2D> ();
					Vector3 playerDirection = player.transform.localScale;
					Vector2 recoilDirection = new Vector2 (playerDirection.x * -10, 1);
					playerBody.AddForce (attackPower * recoilDirection);
				}
			}
			yield return new WaitForFixedUpdate();
		}
		anim.SetBool ("isAttacking", false);
	}

	IEnumerator TakeDamage()
	{
		StopCoroutine ( Idle());
		StopCoroutine ( Roll());
		StopCoroutine ( Attack());

		for (float f = 0; f <= 30; f++)
		{
			yield return new WaitForEndOfFrame();
		}

		anim.ResetTrigger ("isHurt");
		isHurt = false;
	}

	public IEnumerator Idle()
	{
		anim.SetBool ("isAttacking", false);
		anim.SetBool ("isRolling", false);
		energy += 1;
		yield return new WaitForFixedUpdate();
	}

	public IEnumerator Roll()
	{
		energy -= 1;
		robotScale = transform.localScale;
		Vector2 move = new Vector2 (robotScale.x * -1, 0);
		rb.AddForce(move * moveForce);
		rb.velocity = new Vector2 (Mathf.Clamp (rb.velocity.x, -xSpeedLimit, xSpeedLimit), rb.velocity.y);
		yield return new WaitForFixedUpdate();
	}
}
