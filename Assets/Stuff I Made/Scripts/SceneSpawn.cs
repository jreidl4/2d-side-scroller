﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSpawn : MonoBehaviour {

	public int maxPlatforms;

	public float xMin;
	public float xMax;
	public float yMin;
	public float yMax;

	public float Background_zMin;
	public float Background_zMax;

	public float Foreground_zMin;
	public float Foreground_zMax;

	public GameObject[] decorations;

	private Vector3 originPosition;

	private SpriteRenderer spr;

	void Start ()
	{
		originPosition = transform.position;

		SpawnPlatform ();
	}

	void SpawnPlatform()
	{
		for (int i = 0; i < maxPlatforms; i++)
		{
			GameObject decor = decorations[Random.Range (0, decorations.Length)];

			Vector3 randomPosition = new Vector3 (originPosition.x + Random.Range (xMin, xMax), Random.Range(yMin, yMax), Mathf.Clamp(Random.Range (Background_zMin, Background_zMax), Background_zMin, Background_zMax));

			GameObject tree = Instantiate (decor, randomPosition, Quaternion.identity);

			tree.name = tree.name + i;

			if (tree.layer == 9) {
				
				Vector3 moveTree = new Vector3 (tree.transform.position.x, tree.transform.position.y, tree.transform.position.z + 10);
				tree.transform.position = moveTree;
			}

			if (tree.layer == 10) {

				Vector3 moveTree = new Vector3 (tree.transform.position.x, tree.transform.position.y, Random.Range(Foreground_zMin, Foreground_zMax));
				tree.transform.position = moveTree;
			}

				foreach (Transform child in tree.transform)
				{
					
					if (child.tag == "Artwork")
					{
						int sortOrder = 0;
						Transform Artwork = child;

						foreach (Transform ArtChild in Artwork)
						{
							SpriteRenderer spr = ArtChild.GetComponent<SpriteRenderer> ();

						spr.sortingOrder = -Mathf.RoundToInt(tree.transform.position.z) - sortOrder;

							sortOrder += 1;
						}
					}
				}

			Vector3 treeScale = tree.transform.localScale;

			float treeScaleX = Random.Range (0.9f, 1.2f);
			float treeScaleY = Random.Range (0.9f, 1.2f);

			treeScale.x = treeScaleX;
			treeScale.y = treeScaleY;

			tree.transform.localScale = treeScale;

			originPosition = randomPosition;
		}
	}
}
