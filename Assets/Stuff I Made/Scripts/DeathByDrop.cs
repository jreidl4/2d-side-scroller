﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathByDrop : MonoBehaviour {

	public bool DBD_playerDead;

	private GameController gc;
	private Collider2D playerBody;

	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		playerBody = GameObject.Find ("Player").GetComponent<CapsuleCollider2D> ();

		if (gameControllerObject != null)
		{
			gc = gameControllerObject.GetComponent <GameController> ();
		}

		if (gc == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}

	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other == playerBody && DBD_playerDead == false)
		{
			gc.GC_playerActive = false;
			DBD_playerDead = true;
			Debug.Log ("Player Died");
		}
	}

}
